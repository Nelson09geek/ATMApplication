
//***********************************Index.js file for Tops***********************************
'use strict';

//******Express,helmet,bodyparser and cors Library for Creating And Listening Server********************
var express = require('express');
var cors = require('cors');
var helmet = require('helmet');
var bodyParser=require('body-parser');
var app = express();


app.options('*', cors());

app.use(express.static('public'));
app.use(bodyParser.urlencoded({limit: '50mb',extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use('/', express.static(__dirname + '/public/'));
var server=app.listen(process.env.PORT || 3000,function(session){ //express server listening on 3000 port
  console.log("Started on PORT 3000");
});

//**************************Connecting the MongoDB Database***********************************
var database="";
var MongoClient = require('mongodb').MongoClient;
var mongo=require('mongodb'); // for converting string to ObjectID
MongoClient.connect("mongodb://localhost:27017/TopsInfo", function(err, db) {
  if(!err) {
    console.log("We are connected with MongoDB");
  }
  else {
    console.log(err);
  }
  database=db;
});
//*********************************************************************************************



//***************************************API section*******************************************



////step1 check api
app.post('/step1Check',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	
	database.collection('card_detail').find({'card_no':req.body.card_no,'pin':req.body.pin}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				console.log(thresholds);
				if(thresholds.length==1)
				{
					res.json({"flag":"success","data":{"card_id":thresholds[0]._id,"card_balance":thresholds[0].total_balance}});
				}
				else
				{
					console.log("Unauthorized access");
					res.status(403).json();
				}
			}
			
		});
		
});


////step2 check api
app.post('/step2Check',function(req,res){
	
	res.header("Access-Control-Allow-Origin","*");
	var withdraw_amount=req.body.withdraw_amount;
	var atm_balance,nof_2000,nof_500,nof_100,atm_raw_id,card_raw_id;
	var updated_atm_balance,updated_card_balance,up_nof_2000,up_nof_500,up_nof_100;
	var two_note_used=0,five_note_used=0,hund_note_used=0;
	var card_raw_id=parseInt(req.body.card_raw_id),Iflag=0;
	database.collection('atm_detail').find({}).toArray(function(err,thresholds){
			if(err){
				console.log(err);
				res.json({"flag":"error"});
			}
			else{
				console.log(thresholds);
				atm_raw_id=new mongo.ObjectID(thresholds[0]._id);				
				atm_balance=thresholds[0].total_atm_balance;
				var nof_2000=thresholds[0].two_note;
				var nof_500=thresholds[0].five_note;
				var nof_100=thresholds[0].hund_note;
			  if(withdraw_amount<=atm_balance)
			  {
				////Logic goes here
				if(withdraw_amount>=2000)
				{
					
					two_note_used=parseInt(withdraw_amount/2000);	
					if(two_note_used<=nof_2000)
					{
						if((withdraw_amount%2000)>=500)
						{
							console.log("*******************");
							five_note_used=parseInt((withdraw_amount%2000)/500);		
							if(five_note_used<=nof_500)
							{
								hund_note_used=parseInt(((withdraw_amount%2000)%500)/100);
								
								if(hund_note_used<=nof_100)
								{
									
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									 Iflag=1;
								}
							}
							else
							{
								withdraw_amount=withdraw_amount-(two_note_used*2000)-(nof_500*500);
								five_note_used=nof_500;
								hund_note_used=parseInt(withdraw_amount/100);
								console.log("Hund notes  "+hund_note_used);
								console.log(withdraw_amount/100);
								if(hund_note_used<=nof_100)
								{
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
								}
							}
							
						
						}
						else
						{
							hund_note_used=parseInt((withdraw_amount%2000)/100);
							five_note_used=0;
							if(hund_note_used<=nof_100)
							{
									hund_note_used=hund_note_used;
							}
							else
							{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
							}
						}					
					}
					else
					{
						withdraw_amount=withdraw_amount-(nof_2000*2000);
						two_note_used=nof_2000;
						
						if(withdraw_amount>=500)
						{
							five_note_used=parseInt(withdraw_amount/500);
							if(five_note_used<=nof_500)
							{
								hund_note_used=parseInt((withdraw_amount%500)/100);
								if(hund_note_used<=nof_100)
								{
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
								}
							}
							else
							{
								withdraw_amount=withdraw_amount-(two_note_used*2000)-(nof_500*500);
								five_note_used=nof_500;
								hund_note_used=parseInt(withdraw_amount/100);
								if(hund_note_used<=nof_100)
								{
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
								}
							}
							
						
						}
						else
						{
							hund_note_used=parseInt(withdraw_amount/100);
							five_note_used=0;
							if(hund_note_used<=nof_100)
							{
									hund_note_used=hund_note_used;
							}
							else
							{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
							}
						}	
					}
				}
				else
				{
					two_note_used=0;
					if(withdraw_amount>=500)
					{
						five_note_used=parseInt(withdraw_amount/500);
						if(five_note_used<=nof_500)
						{
								hund_note_used=parseInt((withdraw_amount%500)/100);
								if(hund_note_used<=nof_100)
								{
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
								}
						}
						else
						{
								withdraw_amount=withdraw_amount-(nof_500*500);
								five_note_used=nof_500;
								hund_note_used=parseInt(withdraw_amount/100);								
								if(hund_note_used<=nof_100)
								{
									hund_note_used=hund_note_used;
								}
								else
								{
									console.log("ATM has insufficient notes of 100");
									Iflag=1;
								}
						}	
						
					}
					else
					{
						hund_note_used=parseInt(withdraw_amount/100);
						five_note_used=0;
						if(hund_note_used<=nof_100)
						{
							hund_note_used=hund_note_used;
						}
						else
						{
							console.log("ATM has insufficient notes of 100");
							Iflag=1;
						}
					}
				}
				two_note_used=parseInt(two_note_used);
				five_note_used=parseInt(five_note_used);
				hund_note_used=parseInt(hund_note_used);
				console.log("2000 note used "+two_note_used);
				console.log("500 note used "+five_note_used);
				console.log("100 note used "+hund_note_used);
				///////////////////
				up_nof_2000=nof_2000-two_note_used;
				up_nof_500=nof_500-five_note_used;
				up_nof_100=nof_100-hund_note_used;
				//////ATM Update
				if(Iflag==0)
				{
				database.collection('atm_detail').update({'_id':atm_raw_id},{
											$set:{
												    "two_note" : up_nof_2000,
												    "five_note" : up_nof_500,
													"hund_note" : up_nof_100,
													"total_atm_balance" : (atm_balance-req.body.withdraw_amount)
											}},	
											function(err,data){
											if(err){
												console.log(err);
												res.json({"flag":"error"});
											}
											else{
												console.log("******ATM Update Done*******");
												
												
													//card Update
												
													database.collection('card_detail').update({'card_no':card_raw_id},{
														$set:{
															  "total_balance" : (req.body.card_balance-req.body.withdraw_amount)
														}},	
														function(err,data){
														if(err){
															console.log(err);
															res.json({"flag":"error"});
														}
														else{
															console.log("******card Update Done*******");
															console.log(data);
															/////Transaction Insert Entry
															var jsonData={
																	
																	"time" : new Date(),
																	"card_number" : req.body.card_raw_id,
																	"debited_amount" : req.body.withdraw_amount,
																	"last_card_balance" : (req.body.card_balance-req.body.withdraw_amount),
																	"two_note" : two_note_used,
																	"five_note" : five_note_used,
																	"hund_note" : hund_note_used
															};
															database.collection('transaction_detail').insert(jsonData,function(err,data){
															   if(err){
																	console.log(err);
																	
															   }
															   else
															   {
																 console.log("******Transaction Update Done*******");
																 console.log(data);
																 //Return Inserted Table Id
																 var left_balance=req.body.card_balance-req.body.withdraw_amount;
																 console.log(left_balance);
															     res.json({"flag":"success",data:{"left_balance":left_balance,"two_note_used":two_note_used,"five_note_used":five_note_used,"hund_note_used":hund_note_used}});
															   
															   }
															
															});
														}
													});
											}
				});
				}
				else
				{
					res.json({"flag":"insufficent100"});
				}
			 }
			else
			{
					res.json({"flag":"insufficentatm"});
			}			
		  }
			
		});
		
});




