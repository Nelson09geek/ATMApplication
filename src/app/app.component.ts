import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private _router:Router) { }

  ngOnInit(){ 
    
    var card_id=window.sessionStorage.getItem("card_id");
      if(card_id)
      { 
        this._router.navigate(['/step2']); 
      }
      else
      {
        window.sessionStorage.clear();
        this._router.navigate(['/step1']);
      }
  }
}
