import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-step-1',
  templateUrl: './step-1.component.html',
  styleUrls: ['./step-1.component.css']
})
export class Step1Component implements OnInit {

  private cardNo;
  private pinNo;
  constructor(private _router:Router,private _http:HttpClient) { }

  validateCard(){
    console.log(this.cardNo);
    console.log(this.pinNo);
    var pinInt=parseInt(this.pinNo);
    var  url='http://localhost:3000/step1Check';
    this._http.post<any>(url,{
      card_no:this.cardNo,
      pin:pinInt 
     
    })
    .subscribe(
      data => {         
        console.log(data);
        if(data.flag=="success")
        {
          window.sessionStorage.setItem("card_id",this.cardNo);
          window.sessionStorage.setItem("card_balance",data.data.card_balance);
          this._router.navigate(['/step2']); 
        } 
        
        
      },      
      err => {
        var err=<any>err;
        if(err.status==403)
        {
          alert("You are not authorized");
          console.log(err.status);
         
        }
        else
        {
          alert(err);
          console.log(err);
        }
        
       
      }
    );
    

  }
  ngOnInit() {
    window.sessionStorage.clear();
  }

}
