import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-step-3',
  templateUrl: './step-3.component.html',
  styleUrls: ['./step-3.component.css']
})
export class Step3Component implements OnInit {

  private two_note_used;
  private five_note_used;
  private hund_note_used;
  private left_balance;

  constructor(private _router:Router) { }

  ngOnInit() {
    this.two_note_used=window.sessionStorage.getItem("two_note_used");
    this.five_note_used=window.sessionStorage.getItem("five_note_used");
    this.hund_note_used=window.sessionStorage.getItem("hund_note_used");
    this.left_balance=window.sessionStorage.getItem("left_balance");
  }

}
