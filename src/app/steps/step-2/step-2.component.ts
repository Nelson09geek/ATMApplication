import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-step-2',
  templateUrl: './step-2.component.html',
  styleUrls: ['./step-2.component.css']
})
export class Step2Component implements OnInit {
 
  private withdrw_amount:number;
  private card_balance;
  constructor(private _router:Router,private _http:HttpClient) { }
  
  withDrawAmount(){
    
    if(parseInt(this.card_balance)>=this.withdrw_amount)
    {
        if((this.withdrw_amount % 100)==0)
        {
              var card_id=window.sessionStorage.getItem("card_id");
              var  url='http://localhost:3000/step2Check';
              this._http.post<any>(url,{
                withdraw_amount:this.withdrw_amount,
                card_raw_id:card_id,
                card_balance:parseInt(this.card_balance) 
              
              })
              .subscribe(
                data => {         
                  console.log(data);
                  if(data.flag=="success")
                  {
                    window.sessionStorage.setItem("two_note_used",data.data.two_note_used);
                    window.sessionStorage.setItem("five_note_used",data.data.five_note_used);
                    window.sessionStorage.setItem("hund_note_used",data.data.hund_note_used);
                    window.sessionStorage.setItem("left_balance",data.data.left_balance);
                    this._router.navigate(['/step3']); 
                  }
                  else if(data.flag=="insufficent100")
                  {
                      alert("ATM has insufficient notes of 100");  
                  }
                  else if(data.flag=="insufficentatm")
                  {
                      alert("ATM has not sufficient funds");  
                  }
                  else if(data.flag=="error")
                  {
                      alert("Error while Doing Transactions");  
                  }
                  
                },      
                err => {
                  var err=<any>err;
                  if(err.status==403)
                  {
                    alert("You are not authorized");
                    console.log(err.status);
                  
                  }
                  else
                  {
                    alert(err);
                    console.log(err);
                  }
                  
                
                }
              );
        }
        else
        {
          alert("Enter Amount in Multiply of 100");
        }      
    }
    else
    {
        alert("Insufficient Balance");
    }   
  }
  ngOnInit() {

    this.card_balance=window.sessionStorage.getItem("card_balance");
   
  }

}
