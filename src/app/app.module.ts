import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { Step1Component } from './steps/step-1/step-1.component';
import { Step2Component } from './steps/step-2/step-2.component';
import { Step3Component } from './steps/step-3/step-3.component';


@NgModule({
  declarations: [
    AppComponent,
    Step1Component,
    Step2Component,
    Step3Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
       {path:'step1',component:Step1Component},
       {path:'step2',component:Step2Component},
       {path:'step3',component:Step3Component},        
       {path:'',component:Step1Component},
       {path:'**',component:Step1Component} 
      

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
